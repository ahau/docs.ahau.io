# Āhau Download and Installation Guide

Congrats - you're ready to start with Āhau. Please use the following steps to download the Āhau application and install it on your laptop or computer. 

  ## Windows installation guide

  *If you are on a Apple or Linux machine please see find those installation instructions below* 

  [<img src="_media/windows.svg" alt="Windows" width="150"/>](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Windows.exe)

  [Click here to download for Windows](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Windows.exe)

  - Your download will start in the bottom download bar of your browser.
  - Once download is complete, click on the file in the bottom download bar. 
  - Āhau will now start installing. When this is finished the Ahau application should start 
  ### Ka mau te wehi! You've successfully downloaded and installed Āhau onto your computer

  <img src="_media/Screen_Shot_2020-11-03_at_1.06.33_PM.png" alt="Āhau opening screen" width="600"/>

  Now that you've successfully installed Āhau, head over to the [Getting started guide](./get_started) to get started creating your own profile and connect to the Pātaka. 

  If you have any pātai or run into any problems, please get in touch with us at [info@ahau.io](mailto:info@ahau.io) or [chat.ahau.io](https://chat.ahau.io/channel/help). 

----
  ## Apple installation guide


  [<img src="_media/apple.svg" alt="Mac" width="150"/>](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Mac.dmg)

  [Click here to download for Mac](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Mac.dmg)

  - Your download will start in the bottom download bar of your browser.
  - Once download is complete, click on the file in the bottom download bar. A download window will pop up like the one below.
  
  <img src="_media/Screen_Shot_2020-11-03_at_1.05.14_PM.png" alt="Mac application download dialogue box" width="300"/> 


  - Drag Āhau into the **Applications** folder in the **same** download window.
  
  ### You'll now be able to see Āhau in your **Applications** folder

  <img src="_media/Screen_Shot_2020-11-03_at_1.06.03_PM.png" alt="Image shows Āhau in Applications folder" width="600"/>

  ### Open Āhau

  To open the application, double click on Āhau. You’ll receive a warning **(see below image)**. This is standard for apps in Beta Testing mode, and is completely safe for your computer. Click **Open**.

  <img src="_media/Screen_Shot_2020-11-03_at_1.06.12_PM.png" alt="Āhau warning dialogue box" width="600"/>

  ### Ka mau te wehi! You've successfully downloaded and installed Āhau onto your computer

  <img src="_media/Screen_Shot_2020-11-03_at_1.06.33_PM.png" alt="Āhau opening screen" width="600"/>

  Now that you've successfully installed Āhau, head over to the [Getting started guide](./get_started) to get started creating your own profile and connect to the Pātaka. 

  If you have any pātai or run into any problems, please get in touch with us at [info@ahau.io](mailto:info@ahau.io) or [chat.ahau.io](https://chat.ahau.io/channel/help). 

   ## Linux installation guide

  *If you are on a Apple machine please see the Apple installation instructions below* 

  [<img src="_media/linux-icon.webp" alt="Linux" width="150"/>](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Linux-x86_64.AppImage)

  [Click here to download for Linux](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Linux-x86_64.AppImage)

  - Your download will start in the bottom download bar of your browser.
  - Once download is complete, click on the file in the bottom download bar. 
  - Āhau will now start installing. When this is finished the Ahau application should start.
  ### Ka mau te wehi! You've successfully downloaded and installed Āhau onto your computer

  <img src="_media/Screen_Shot_2020-11-03_at_1.06.33_PM.png" alt="Āhau opening screen" width="600"/>

  ## Android installation guide

  If you would like to download our **ALPHA** mobile app available for Android users you can download and install the application via the internet. 
  :warning: Please note that as of 17th December, release 2.25.6: Is early realease has some known issues connecting and syncing with Pataka. If you are experiencing this issue please check back in a couple weeks for a new release.

  [<img src="_media/Android.png" alt="Android" width="150"/>](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Android.apk)

  [Click here to download for Android](https://github.com/Ahau-NZ/Ahau/releases/latest/download/Ahau_Android.apk)

  - Your download will start in the background.
  - Once download is complete, you may be prompted to update your settings to allow installation of apps from the internet or storage. Once enabled you will be asked if you would like to install the Ahau application.
  - Āhau will now start installing. When this is finished the Ahau application should start 
  ### Ka mau te wehi! You've successfully downloaded and installed Āhau onto your computer

  <img src="_media/Android-home.jpg" alt="Āhau opening screen" width="300"/>

  Now that you've successfully installed Āhau, head over to the [Getting started guide](./get_started) to get started creating your own profile and connect to the Pātaka. 

  If you have any pātai or run into any problems, please get in touch with us at [info@ahau.io](mailto:info@ahau.io) or [chat.ahau.io](https://chat.ahau.io/channel/help). 
