# Creating records 

A *record* in Āhau is a recorded entry saved to either your personal archive, or a tribal archive. It is a way to document an important piece of information that you want to be recorded and remebered in a digital format. This can be stories about your life or the life of an ancestor or relative, or some information or knowledge important to your tribe. Some examples might include things like past events, important korero, stories and histories, waiata and karakia, etc. 

*Important:* Although you can create private records in your personal archive, if you want others to access a record it must be created in a Tribe. You can navigate between Tribes via the top navigation bar. If you do not have any Tribes available, try [joining](joining_guide) a or [creating one](community_guide) for you and your whānau.
## Creating a record

- Click *Archive* on the navigation menu.

-  Click the *+* symbol

<img src="_media/create_record_1.gif" width="580" height="344"/>

This will prompt the record window to open.

- Add record details to your record

<img src="_media/Screen_Shot_2020-11-09_at_12.53.11_PM.png" width="600"/>

A basic record includes a *title*, *description of the event*, and *date* that the event occurred. The more detail you are able to record the better, although you are able to update the record at any time to ensure it remains accurate.

## Adding artefacts

<img src="_media/create_record_2.gif" width="580" height="344"/>

*Artefacts* are media assets associated with a record. 

You're able to upload multiple images, video files, and documents to support the record. Currently there is a *5MB limit* on upload file size. All common photo, video and audio file formats are supported. 
You can also record details about each artefact including *title*, *description, date, source, file size* and other information.
You're able to add, edit or delete artefacts at any point after the record is created. 

### Add location

<img src="_media/create_record_3.gif" width="580" height="344"/>

Add a description about the location or place associated with the record.  

### Mentions

*Mentions* are prople who are mentioned in a record, such as a story that involves an ancestor or relative, or a group of people. You can mention anyone listed on a whakapapa record that exists in your archive. This makes it easier to find records about that person when [exploring your whakapapa](./whakapapa_guide.md).

### Contributors

*Contributors* is the person who *contributed* or entered the record into the Archive. You may also want to mention other people who helped contribute to the recording or to the knowledge provided within a record 

### Advanced

<img src="_media/create_record_4.gif" width="580" height="344"/>

### Related records
When you have created several records, some of them may be closely connected. You are able to add any closely linked records in order to guide viewers from one record to another. 

### Creators
Add creators, or people who are responsible for providing the essential knowledge that goes into the record.  

### Kaitiaki
The Kaitiaki is the creator of the record. Only the Kaitiaki is able to edit and delete the record.

### Details
For more detailed archival records, please include information about the file format, identifier, source, language and translation/transcription. 

## Seting access and permissions 

<img src="_media/cultural_record_5.png" alt="Āhau opening screen" width="600"/>

The access to a record is set based in the Tribe that you are in. Currently stories are viewable by everyone in the Tribe. 
- If you are writing a story that you would like others to contribute too you can change the permission to *'edit'*


## Updating and edit your record 

<img src="_media/create_record_final.gif" width="580" height="344"/>

  Only a kaitiaki of a record can edit, or delete a record. If the record needs to be edited, you are able to do so at any time.

--------
If you have any pātai or run into any problems, please get in touch with us at [info@ahau.io](mailto:info@ahau.io) or [chat.ahau.io](https://chat.ahau.io/channel/help). 

