# Installing and setting up your community server (Pātaka)

## What is a Pātaka?

A Pātaka (or Pātaka taonga) is your **community database server**, which is just another computer running a database that is usually connected to the internet. Because it is connected to the internet you and your tribe can store and share information with each other via this **community database server** or in our case the **Pātaka**.

A Pātaka can also be run **offline** (without an internet connection). It wont be able to connect with other people over the internet but it can still store and share information with people who connect to the same local wifi connection (we call this digital kānohi-a-kānohi). This is mainly for areas that are unable to connect to the internet. But this is also useful if you would like to create a local archive, meaning you can still store and share information locally but don't necessarily want it to be accessible via the internet. [Click here to skip ahead to create a Local Pātaka (that is offline, aka not connected to the internet)]().      

To make this setup easy we have built a stand-alone application called **Pātaka**. Anyone can build a Pātaka and connect it to the internet using **port-forwarding**, to provide a community server for you and your tribe to connect to.

All information‍ held on a Pātaka is fully encrypted providing a high level of security and privacy. The only way to access information is by providing the access key which is stored on your personal computer or device.  

<img src="_media/replication-via-internet_css.svg" alt="Āhau Pātaka" width="500"/>

## What is the Public Pātaka?

For some communities, setting up a Pātaka may not be possible because they dont have a device that they can dedicate running the Pātaka application, or this might be too much of a challenge to setup or manage. 
To support these communities the Āhau development team have setup a Public Pātaka called Pātaka Aotearoa. This Pātaka is available for anyone to connect to in Āhau, and is available to host your communities data online free of charge.

To connect to Pataka Aotearoa simply click the **+ join network** button either from your home profile page or from the tribes page.

Once the dialog opens select the second option to connect to the Public Pātaka, agree to the terms and click connect.

For those communities where hosting your own data is important to your data soveriegnty or where the internet is not accessible read on to learn how to setup your own Pātaka and host your own data.

<img src="_media/public-pataka.PNG" alt="Public Pātaka" width="500"/>

You should now see a **successful connection message** and you Āhau application will start to disconver the different tribes that are hosted on the Public Pātaka.

You try to find your tribe here and send a registration request, or any tribe that you create will now also be available on this to others to join after they have connected to the Public Pātaka. 

## Setting up an online Pātaka

### Equipment

- [ ] Computer
- [ ] Router 
- [ ] Internet connection
- [ ] Static IP address (depending on your internet provider you may already have one by default, if not, you may need to ask your internet provider for one)

### 1. Setting up your static IP address 


An IP (Internet Protocol) address is like a digital street address for computers and devices so that they can locate and connect to each other. Both your internet connection and your computer have IP addresses. 

These addresses can change, this is called a **dynamic IP** address.

A **static IP** address is an address that does not change. We need a static IP address for your Pātaka so that when you look for your Pātaka on the internet you can locate it everytime.

#### 1.1 Setting up static IP address for your internet connection
To setup your Pātaka, first make sure you have a static IP address setup. To do this please call your internet provider and request to have a static IP address setup for your connection (depending on your internet service provider there may be an extra charge for this. Usually $10 per month).

#### 1.2 Setting up static IP address for your device

For a Mac:
- Click the Apple icon then select the *System Preferences*

 <img src="_media/setup_ip_4.png" alt="System Preferences" width="500"/>

- Under *Internet & Wireless*, select *Network*.
- Click the *Advanced* button when the *Network* screen appears. The default Location is Automatic.
 <img src="_media/setup_ip_3.png" alt="Advanced settings" width="500"/>
- Click TCP/IP
 <img src="_media/setup_ip_2.png" alt="TCP/IP" width="500"/>
- Copy your IPv4 Address
- From the *Configure IPv4* drop-down list, select *Manually*
- Enter your an IP address copied in the previoues step into the IPv4 field.
 <img src="_media/setup_ip.png" alt="Enter IP address" width="500"/><br>
NOTE:  Make sure that the IP address you’ll be assigning the computer has the same first three (3) numbers as the Default Gateway, and the last 3 digits should be between 1-254. 

*Make a note of the IP address you entered. We will need this address later when setting up port forwarding.*

- Click *OK*
- Click *Apply*

### 2. Setting up Port-forwarding

#### 2.1 What is port forwarding?

`Port forwarding is the process of intercepting traffic bound for a certain IP/port combination and redirecting to a different IP and/or port.`

What does this mean?

The static IP from your internet provider will help your tribe connect to your location (your house, marae, or office). We will now need to figure out which computer at your location is the Pātaka, as you may have several computers or devices connected to the internet in your location at any point in time. 

This is where portforwarding comes in. 

We need to setup **port forwarding** on your modem router (the device provided by your Internet provider which sonnect your household/office/marae to the internet) so that when a incoming connection for you Pātaka arrives to your location, your router will know which computer to send the incoming connection too.

 <img src="_media/port-forwarding.svg" alt="Port forwarding" width="500"/>

#### 2.2 Set up port forwarding on your device

The process for setting up Port forwarding differs for each router. 

To complete the process you will need to know 
- [ ] what name or type of router you have at your location 
- [ ] the login details for your router 

Both pieces of this information can usually be found on router itself (look for a sticker)  

Once you have this information, please follow this guide to find your router and setup port forwarding to your device. https://portforward.com/router.htm. 


In this example I'm using a Spark router to create a port forwarding entry. Your router may vary slightly but should follow the same general process. 

1. Login to your router.

To do this you need your router's IP address. To find your router IP address, follow these [instructions](https://www.businessinsider.com/how-to-find-ip-address-of-router?r=AU&IR=T).

2. Navigate to your router's port forwarding section. 

3. Create the port forwarding entries in your router.

- Select *New Port Mapping*
- Mapping name: Pātaka
- For *Application* select *Add port mapping application* 
- You need to add a new *Āhau Pātaka* application, provide the following details:
    Name: Āhau Pātaka
    External Port: 8088 
    Internal Port: 8088
    Protocol: TCP
- *Save*
- Internal host: (select your device)
- *Save*

4. Test that your port forwards correctly using the [Port Forwarding Test](https://www.portforwarding.org/). 

## Download and install the Pātaka

Once you have port-forwarding setup download the Pātaka and install it on a computer that you want to dedicate to being a Pātaka. Keep in mind that for the Pātaka work it needs to be connected to the internet in the same place that you have setup your port forwarding and it needs to remain powered on. If you lose connection or power its not a major issue, it just means that people wont be able to sync with the Pātaka until it is powered back on and re-connected to the internet. 

Downloading and installing the Pātaka will be a similar process to downloading and installing Āhau. 

Download the Pātaka application

  | [<img src="_media/apple.svg" alt="Mac" width="100"/>](https://github.com/Ahau-NZ/Pataka/releases/download/v4.0.0/pataka-Mac-4.0.0.dmg) | [<img src="_media/windows.svg" alt="Windows" width="100"/>](https://github.com/Ahau-NZ/Pataka/releases/download/v4.0.0/pataka-Windows-4.0.0.exe) | [<img src="_media/linux-icon.webp" alt="Linux" width="100"/>](https://github.com/Ahau-NZ/Pataka/releases/download/v4.0.0/pataka-Linux-4.0.0-x86_64.AppImage) |
  | --- | --- | --- |
  | [Click here to download for Mac](https://github.com/Ahau-NZ/Pataka/releases/download/v4.0.0/pataka-Mac-4.0.0.dmg) | [Click here to download for Windows](https://github.com/Ahau-NZ/Pataka/releases/download/v4.0.0/pataka-Windows-4.0.0.exe) | [Click here to download for Linux](https://github.com/Ahau-NZ/Pataka/releases/download/v4.0.0/pataka-Linux-4.0.0-x86_64.AppImage) |

### Create your Pātaka

Once you have Pātaka installed, open the application and you will be prompted to create a Pātaka, giving it a name, a description and an image. Your Pātaka will now run some computer and network checks to see if it has port-forwarding setup correctly and that there is some free space on the computer.  
If everything is setup correctly you should expect to see 3 green-lights next to port-forwarding, online and local access.  

#### Your Pātaka is now setup and is ready for you to generate invite codes and send to your tribal members and sync with. 

## Connecting to the Pātaka 

Once your Pātaka is set up, you and your whānau can now connect Āhau to the Pātaka and join a Tribe. 

*As a Pātaka owner:*
- Generate a *'join code'*
    NOTE: For safety the join code can either be a single use code, meaning the code can only be used once, or a multiple use code meaning that many people can use the same code to join the Pātaka.
    The Pātaka code will look something like this `pataka.ahau.io:8088:@+bhdksbayh8jkld7y8fdsa980dfdsnaj889sd=.ed25519~njfkdsbyahu89dsaui89fdsanu89asji0dsau90ds=`
- Once the join code is generated, copy the code and send it to the person/people that you want to connect to the Pātaka
- Once they have recieved the code, they need to head to the *Tribes* page by clicking on the *'+'* button in the top navigation menu
 <img src="_media/pataka_connect_3.png" alt="Connect to Pātaka" width="500"/>

- Click on the *'+ JOIN NETWORK'* button in the Pātaka section on the right and box will popup 
  
  <img src="_media/pataka-dialog.png" width="600"/>

- Copy the whole *PĀTAKA CODE* and paste it in this box and click *'Connect'*
- If everything went well you will see a little box appear at the bottom of the screen saying "successfully connected to Pātaka"
- Your computer will now start syncing information from the Pātaka, **this may take some time**.

If this doesnt happen or you get an error, it means that something has gone wrong and is likely an issue with the Pātaka or your own internet connection. Feel free to [connect with our team here](https://chat.ahau.io/channel/help) and ask for help and we will be able to help you out from there. 

- When syncing is complete you will now see any Tribes connected to that Pātaka. 
- You can click on one to send a request to join
- You may also create a new Tribe for your Whānau, Hapū, Marae or Iwi community. See this guide for more info on setting up a new Tribe [How to create a tribe](commuinty_guide)

## Setting up an offline Pātaka 

The process for setting up an offline Pātaka is a simialr process to setting up an online Pātaka except you can skip steps 1 and 2
