# Kia ora
# Haere mai and welcome to Āhau guides. 

Here you will find information and guides about setting up and using the Āhau application. If you get stuck with anything or have further question that aren't listed here please jump into the help channel in [chat.ahau.io](https://chat.ahau.io/channel/help) and someone from our team will be there to answer your questions.

Ngā mihi nui kia koutou

# What is Āhau

Āhau is a Whānau Data Platform that helps whānau-based communities (whānau, hapū, Iwi) capture, preserve, and share important information and histories into secure, whānau managed databases and servers.

With Āhau, you’re able to keep track of whānau whakapapa information, build you own tribal registries preserve and share cultural records and narratives, own and control whānau data and servers, and build a stronger sense of whānau, community and identity.

To learn more [visit our website](https://ahau.io) or feel free to contact us today at info@ahau.io or chat with us at [chat.ahau.io](https://chat.ahau.io/channel/help) so that we can see how we can best assist your needs and support your journey with Āhau.

[Click here to started with the Ahau Setup Guide](./download_guide) <br>

[Click here to download Āhau User Guide PDF](https://drive.google.com/file/d/14JyZF6pri-ltjaRvI97MRf4U9TtOGqcv/view?usp=sharing)