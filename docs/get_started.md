# Getting started with Āhau

  The first step in your journey with Āhau is installing and downloading Āhau, if you haven't completed this yet head over to the Āhau Download Guide. 

  Once you've got Āhau installed its time to *get started* by setting your personal profile and private archive

  ## Create your own profile 

  When you create a profile in Āhau it also creates a new private and fully ecrypted archive. This archive and all the information that you save as *private* when creating records is saved and ecrypted to your personal archive and can only be accessed by your computer or device.
  
  <img src="_media/Screen_Shot_2020-11-03_at_1.06.33_PM.png" alt="Āhau opening screen" width="600"/>

  *Click* on *'KO WAI KOE -- WHO ARE YOU?'*

  ## Fill in your profile details

  <!-- ![Āhau Profile form](https://gitlab.com/ahau/docs.ahau.io/-/blob/master/docs/_media/Screen_Shot_2020-11-03_at_1.09.24_PM.png) -->
  <img src="_media/Screen_Shot_2020-11-03_at_1.09.24_PM.png" width="600"/>

  Fill in all the relevant details for your profile information. Please note that none of the fields are mandatory, you only need to complete what you feel comfortable sharing, you can also edit your profile at any point in time. 

  NOTE:
  - *birth order* refers to whether you are the first, second, third, etc. child in your whānau. If you are the first birth order = 1, second child birth order = 2, etc. 
  - Your 'legal name' is your full name as it would be written on your passport or driver's license
  
  
  Your profile information is private, and will only be shared with those in your Tribes. 

## Connecting to a Pātaka

  Once you have setup your profile you will be prompted to connect to a Pātaka (community server), if you have recieved a Pātaka code this is where you put it in. 

  <img src="_media/pataka-dialog.png" width="600"/>

  
  If you dont have a Pātaka code or haven't set up a Pātaka for your community yet please refer to the [community server setup guide](pataka_guide)

  Otherwise you can skip this step for now and come back to it later, or you can connect to the Public Pātaka. Please read the different options for more information.



#### Success 
  
  Once you have successfully setup your profile on your computer you will now have 
  - a database on your computer to store your Āhau information 
  - A personal encryption key setup on your device 

  Now here a couple of other things to try

  - [create a tribe ](community_guide)
  - [create an archive record](records_guide)
  - [create a whakapapa record](whakapapa_guide)
  
  If you have any pātai or run into any problems, please get in touch with us at [info@ahau.io](mailto:info@ahau.io) or [chat.ahau.io](https://chat.ahau.io/channel/help). 
