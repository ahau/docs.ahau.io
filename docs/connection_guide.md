# Connecting to your community server (Pātaka)

## What is a Pātaka?

A Pātaka (or Pātaka taonga) is your **community database server**, which is just another computer running a database that is usually connected to the internet. Because it is connected to the internet you and your tribe can store and share information with each other via this **community database server** or in our case the **Pātaka**.

All information‍ held on a Pātaka is fully encrypted providing a high level of security and privacy. The only way to access information is by joining a Tribe that is connected to the same community Pātaka.  

If you dont want to setup your own community Pātaka you can either use an existiing community Pataka or the Public Pātaka. [See this link to learn more](pataka-guide?id=what-is-the-public-pātaka). 

<img src="_media/replication-via-internet_css.svg" alt="Āhau Pātaka" width="600"/>

## Connecting to the Pātaka 

Once your community Pātaka is set up, you and your community members can now connect Āhau to the Pātaka and join Tribes hosted on the Pātaka. 

- Once they have recieved the code, they need to head to the *Tribes* page by clicking on the *'+'* in the top navigation menu
 <img src="_media/pataka_connect_3.png" alt="Connect to Pātaka" width="500"/>

- Click on the *'+ JOIN NETWORK'* button in the Pātaka box on the right and a box will popup 
  
  <img src="_media/pataka-dialog.png" width="600"/>

- Copy the whole *PĀTAKA CODE* and paste it in this box and click *'Connect'*
- If everything went well you will see a little box appear at the bottom of the screen saying "successfully connected to Pātaka"
- Your computer will now start syncing information from the Pātaka, this may take some time.

If this doesnt happen or you get an error, it means that something has gone wrong and is likely an issue with the Pātaka or your own internet connection. Feel free to [connect with our team here](https://chat.ahau.io/channel/help) and ask for help and we will be able to help you out from there. 

- When syncing is complete you will now see any Tribes connected to that Pātaka. 
- You can click on one to send a request to join
- You may also create a new Tribe for your community. Including your Whānau, Hapū, Marae or Iwi.

See this guide for more info on setting up a new Tribe [How to create a tribe](commuinty_guide)
