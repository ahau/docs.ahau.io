# Creating Digital Tribal Membership Credentials

This feature is **still in BETA and is recommeneded to be used for community experimentation, demonstration and research only**.
If you would like to officially implement this solution with you community its important to discuss with you community
- your verification and approval processes
- what information is important to be on a your membership credentials
- how membership credentials can and should be used
- digital accessibility

For any support setting up your registration and verification process please contact <a href="mailto:identity@ahau.io">identity@ahau.io</a>


## What is a Digital Tribal Membership Credential?

<img src="_media/credential.jpeg" alt="Show credentials" width="300" style="border: solid lightgrey"/>

A Digital Tribal Membership Credential is just like a physical membership card for your tribe, except that it lives inside the Ahau application on your phone or computer. 

The Digital Tribal Membership Credentials use the latest Cryptographic Identity technologies, including Decentralised Identifiers and Verifiable Credentials, giving tribal communities and individuals access to reliable, safe and secure identity managment infrastructure.

Digital Tribal Membership Credentials can be used by individuals, tribal entites and organisations to verify someones identity and to verify their verified membership as a part of a tribe, which inturn could be used to verify someones rights or entitlements to services and benefits as a verified member of a tribal and indigenous community.

For a brief explanation of Decentralised Identitiers and Verifiable Credentials we recommend you 
<a href="https://www.youtube.com/watch?v=Ew-_F-OtDFI">
  check out this following video by Microsoft
</a>
</br>


## How do Digital Tribal Membership Credentials work in Āhau?

Kaitaiki can setup there tribe(s) to issue Digital Tribal membership credentials to new members when a members request to join a tribe has been verified and approved by a Kaitiaki.

The member will be able to see the new **credentials menu** which will show any Digital Tribal Membership Credentials that have been issued to the person.

Digital Tribal Membership Credentials can then be shared, either directly [with an OOB invitation code](https://docs.atalaprism.io/tutorials/connections/connection#invitee-accepts-oob-invitation) (this requires setting up and managing a verifier agent), or in Ahau to verify new tribal membership requests. If another tribal community is also using Ahau, and has shared membership requirements (such as shared genealogy requirements) than a community can setup thier tribe to recieve tribal membership credentials from other tribal communities.

## How can I create a Digital Tribal Membership Credential for my tribe?

**Pre-requisites:**
1. [Have Ahau installed on your computer](download_guide.md)
2. [Have an Ahau profile](get_started.md)
3. [Have your Tribe setup](community_guide.md)
4. [Be Connected to your Pātaka](connection_guide.md)

Each Kaitiaki that wants to Issue Digital Tribal Membership Credentials for their tribe must do the following.

1. Setup a Hyperlegder Open Enterprise Agent (OEA) to handle the creation and sending of credentials to new members.
To set this up you can either, follow the documentation for <a href="https://github.com/hyperledger-labs/open-enterprise-agent" target="_blank">setting up an Open Enterprise Agent here</a> or you can contact our team via email <a href="mailto:identity@ahau.io">identity@ahau.io</a> for support with the setup process.

Once you have an online agent setup please note down the 
    - `ISSUER_URL`
    - `ISSUER_APIKEY`

2. Open Ahau and navigate to tribe settings 

  <img src="_media/tribe_settings.png" alt="Tribe settings button" width="700"/>

3. Turn on "Beta Features > Membership Credentials > Issue membership credentials" in the tribe settings

 <img src="_media/beta_features.png" alt="Toggle beta features" width="700"/>

3. Note down your tribe ID
  - The easiest way to do this is to open Chrome Dev Tools on the Ahau application
    You can do this using keyboard shortcuts.
    With the Ahau application open press
      - `Cmd + option + i` if using a mac device, or
      - `Ctrl + shift + i`if using a windows machine
    - With the Dev Tools Panel open select the `Console` tab and enter `decodeURIComponent(window.location.href.split('/')[4]`
  - Copy or write the response. It will look some like in the below picture
  
  <img src="_media/get_tribe_id.png" alt="Get tribe id" width="700"/>

3. Close Ahau

We now have all the information and setup needed to edit the Ahau config file and tell it which OEA (setup in step 1) to use for issuing your tribal credentials. 

4. Edit the Ahau config file. 
  The location of the config file will depend on your system OS
    - If using a Mac this can be found `Library\Application Support\ahau\config`
    - If using a Windows machine it can be found `{user}\App Data\Local\ahau\config`

  Open the config file with any text editor. 
  
  You will see something that looks like this:

  ```
  {
    "mixpanelId": "abc123..."
  }
  ```
  
  At the end of the "mixpanelId" line add a `,` and press `enter` to start a new line
  Copy and paste the following, replacing 
    - the `{tribeId.cloaked}`with you tribe Id, and 
    - replacing the tribeName, ISSUER_URL, and ISSUER_APIKEY, with you tribe and OEA details.

  ```
  "atalaPrism": {
    "issuers": {
      "tribeId.cloaked": {
        "tribeName": "Your tribe name",
        "ISSUER_URL": "https://your.agent.address/prism-agent/",
        "ISSUER_APIKEY": "yourApiKey"
      }
    }
  }
  ```

  The end result for your config file will look something like this

  ```
  {
    "mixpanelId": "abc123...",
    "atalaPrism": {
      "issuers": {
        "%bnjkBJKNhiuohjioNioNioIO678Hui9=.cloaked": {
          "tribeName": "Cardano Tribe",
          "ISSUER_URL": "https://verifier.ahau.io/prism-agent/",
          "ISSUER_APIKEY": "jkBH876Bkl908y89bvhj8FhLLb8"
        }
      }
    }
  }
  ```
  
  Save you config file and close the text editor. 

5. Restart Ahau!

Congratulations, you are now all set to issue membership cards to newly approved members who register to join your tribe with Āhau 

## How can I accept a Digital Tribal Membership Credentials with new tribe membership requests?

**Pre-requisites:**
1. [Have Ahau installed on your computer](download_guide.md)
2. [Have an Ahau profile](get_started.md)
3. [Have your Tribe setup](community_guide.md)
4. [Be Connected to your Pātaka](connection_guide.md)
5. Only people who have already been issued a tribal membership credential from a tribe will be able to provide one with their membership request.

Each Kaitiaki that wants to accept Digital Tribal Membership Credentials with new membership requests must do the following.

1. Setup a Hyperlegder Open Enterprise Agent (OEA) to handle the acceptance and verification of credentials recieved from new membership requests.

    *Note: If you already have an agent setup for issuing credentials you can skip this step and use the same URL and API credentials*

    To set this up you can either, follow the documentation for <a href="https://github.com/hyperledger-labs/open-enterprise-agent" target="_blank">setting up an Open Enterprise Agent here</a> or you can contact our team via email <a href="mailto:identity@ahau.io">identity@ahau.io</a> for support with the setup process.

    Once you have an online agent setup please note down the 
      - `VERIFIER_URL`
      - `VERIFIER_APIKEY`

2. Open Ahau and navigate to tribe settings 

  <img src="_media/tribe_settings.png" alt="Tribe settings button" width="700"/>

3. Turn on "Beta Features > Membership Credentials > Accept membership credentials" in the tribe settings

 <img src="_media/toggle_verifier.png" alt="Toggle accept credentials" width="700"/>

4. Note down your tribe ID
  - The easiest way to do this is to open Chrome Dev Tools on the Ahau application
    You can do this using keyboard shortcuts.
    With the Ahau application open press
      - `Cmd + option + i` if using a mac device, or
      - `Ctrl + shift + i`if using a windows machine
    - With the Dev Tools Panel open select the `Console` tab and enter `decodeURIComponent(window.location.href.split('/')[4]`
  - Copy or write the response. It will look some like in the below picture
  
  <img src="_media/get_tribe_id.png" alt="Get tribe id" width="700"/>

5. Close Ahau

We now have all the information and setup needed to edit the Ahau config file and tell it which OEA (setup in step 1) to use for accepting and verifying credentials. 

6. Edit the Ahau config file. 
  The location of the config file will depend on your system OS
    - If using a Mac this can be found `Library\Application Support\ahau\config`
    - If using a Windows machine it can be found `{user}\App Data\Local\ahau\config`

  Open the config file with any text editor. 
  
  You will see something that looks like this:

  ```
  {
    "mixpanelId": "abc123..."
  }
  ```
  
  At the end of the "mixpanelId" line add a `,` and press `enter` to start a new line
  Copy and paste the following, replacing 
    - the `{tribeId.cloaked}`with you tribe Id, and 
    - replacing the tribeName, ISSUER_URL, and ISSUER_APIKEY, with you tribe and OEA details.

  ```
  "atalaPrism": {
    "verifiers": {
      "tribeId.cloaked": {
        "tribeName": "Your tribe name",
        "ISSUER_URL": "https://your.agent.address/prism-agent/",
        "ISSUER_APIKEY": "yourApiKey"
      }
    }
  }
  ```

  The end result for your config file will look something like this

  ```
  {
    "mixpanelId": "abc123...",
    "atalaPrism": {
      "verifiers": {
        "%bnjkBJKNhiuohjioNioNioIO678Hui9=.cloaked": {
          "tribeName": "Cardano Tribe",
          "VERIFIER_URL": "https://verifier.ahau.io/prism-agent/",
          "VERIFIER_APIKEY": "jkBH876Bkl908y89bvhj8FhLLb8"
        }
      }
    }
  }
  ```
  
  Save you config file and close the text editor. 

7. Restart Ahau!

Congratulations, you are now all set to accept and verifier membership cards with new membership requests recieved in Āhau.

## What does it looks like if I want to Issue and Accept Tribal membership credentials

The above processes for [Issuing]() and [Accepting]() credentials are the same except you will have both settings activated in tribal settings

  <img src="_media/both_verifier_issuer_toggled.png" alt="Issuer and verifier active" width="700"/>

And your config file will have both options, and will something link this.

  ```
  {
    "mixpanelId": "abc123...",
    "atalaPrism": {
      "issuers": {
        "%bnjkBJKNhiuohjioNioNioIO678Hui9=.cloaked": {
          "tribeName": "Cardano Tribe",
          "ISSUER_URL": "https://verifier.ahau.io/prism-agent/",
          "ISSUER_APIKEY": "jkBH876Bkl908y89bvhj8FhLLb8"
        }
      },
      "verifiers": {
        "%bnjkBJKNhiuohjioNioNioIO678Hui9=.cloaked": {
          "tribeName": "Cardano Tribe",
          "VERIFIER_URL": "https://verifier.ahau.io/prism-agent/",
          "VERIFIER_APIKEY": "jkBH876Bkl908y89bvhj8FhLLb8"
        }
      }
    }
  }
```
`

## Get a DEMO tribal membership credential

We have setup a DEMO tribe that issues Digital Tribal Membership Credentials so that people can see what this looks like.</br>
**Pre-requisites:**
1. [Have Ahau installed on your computer](download_guide.md)
2. [Have an Ahau profile](get_started.md)

To get this credential please [join the following Pātaka](connection_guide.md) using this Pātaka code `cloud.ahau.io:8088:@5PhEzQLVH5D4l+V0tKA/uaCROIxXo6KTv8hh7YVaepg=.ed25519~Lm5v0GVuT0jxzDCxHphBSMDO9l+K3worQ/e4nwFWUR8=`

Once the Pataka has finished loading. [Send a request to join](joining_guide.md) the "Atala Tribe"

Once your request is approved you will recieve your Cardano tribe membership credential.

## Setup Video Instructions

We have also made a video which shows the setup process. You can view this video here

<iframe src="https://drive.google.com/file/d/1lYZDOmfPlB1D8C5J1F4xssTuGq8uPlBR/preview" width="700" height="620" allow="autoplay"></iframe>

## Cardano & Atalaprism technology

This feature has been funded by the The Project Catalyst Community and the supported by the AtalaPrism team.
The Open Source Identity Infrastructure that this feature leverages has been developed by the AtalaPrism team and adapted by the Ahau team to work with the Ahau and Pataka applications.

A special thanks to the Project Catalyst and AtalaPrism team for all their support. Ngā mihi nui kia koutou katoa
