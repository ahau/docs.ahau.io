# Join your tribe

In Āhau a Tribe is a private group with its own community profile and archive and encryption keys. Community members will be able to access and share information and records. Whakapapa records, profiles and stories created in the tribal archive will be accessible by approved tribal members. Tribes are similar to private groups in social media platforms, except all information is encrypted with a secret tribal key and can only be accessed by tribal members who have been approved and given a copy of the key.

To connect to a tribe you must first be connected to the same network (or Pātaka) that the tribe is on.  

## Connecting to the Pātaka 

To connect to a Pātaka you mist first ask the Pātaka administrator for a connection invitation or code.

- Once you have recieved the code, you then need to head to the *Tribes* page by clicking on the *'+'* button in the top navigation menu
 <img src="_media/tribes.png" alt="Connect to Pātaka" width="700"/>

- Click on the *'+ JOIN NETWORK'* button in the Pātaka section on the right and box will popup 
  
  <img src="_media/pataka-form.png" width="700"/>

- Copy the whole *PĀTAKA CODE* and paste it in this box and click *'Connect'*
- If everything went well you will see a little box appear at the bottom of the screen saying "successfully connected to Pātaka"
- Your computer will now start syncing information from the Pātaka, **this may take some time**.

If this doesnt happen or you get an error, it means that something has gone wrong and is likely an issue with the Pātaka or your own internet connection. Feel free to [connect with our team here](https://chat.ahau.io/channel/help) and ask for help and we will be able to help you out from there. 

- When syncing is complete you will now see any Tribes connected to that Pātaka. 
- You can click on the tribe you would like to join and then click on the `join tribe` button 

  <img src="_media/join-tribe.png" width="700"/>

- a box will open asking you to update your profile with any required information to join the tribe.
- once you have updated your profile you will also need to agree to sharing your community profile information with other tribal members, and your private contact information with tribal administrators

  <img src="_media/join-form.png" width="700"/>

- once you have agreed to this you can then submit your registration request along with a message to administrators.

If you cannot find your tribe on the Pātaka you may need to connect to other Pātaka or in the event that a tribe does not yet exist, you may also create a new Tribe for your tribe or family community. See this guide for more info on setting up a new Tribe [How to create a tribe](community_guide)
