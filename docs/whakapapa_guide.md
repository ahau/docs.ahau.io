# Create a Whakapapa Record

In Āhau and in Māori culture a whakapapa record is a genealogy chart.

Creating a Whakapapa record is an essential part of recording the history of your whānau. Many of us will have kaumātua or kuia, aunties or uncles who are the Pou Mātauranga for our whānau, holding decades (or more) of whānau taonga and mātauranga. As Māori, our whakapapa is recognised as our direct line to our ancestors, an acknowledgement of who we are and where we come from and how we connect to each other. 

In Āhau the whakapapa records feature are the core feature of the app, it is used to build the tribal registry, connect and find records in archives, and help kaitiaki to determine who is a part of a tribe.  

In this example we will create a small whakapapa for the Peri whānau.

*Important:* Although you can create private records in your personal archive, if you want others to access a record it must be created in a Tribe. You can navigate between Tribes via the top navigation bar. If you do not have any Tribes available, try [joining](joining_guide) a Tribe or [creating one](community_guide) for you and your whānau.

## Navigating to the Whakapapa page

To navigate to the Whakapapa feature click on *whakapapa* on the left side menu. This will take you to a list of whakapapa for the current profile you are in. You can also use the top navigation bar to go to a list of all the whakapapa records that you have access to.

<img src="_media/whakapapa_1.png" alt="Āhau profile screen" width="600"/>

## Create a new whakapapa record

Click the *+* button in the top right corner.

### Fill in details about your new record

<img src="_media/whakapapa_3.png" alt="Āhau profile screen" width="600"/>

Give your whakapapa record a name and a concise description, for example, if your whakapapa record is intended just for your immediate whānau. 

Upload a suitable photo e.g. your Marae or Whare, or a member of your whānau. It can be anything, so whatever you feel is reflective of your whānau. 

<img src="_media/whakapapa_4.png" alt="Āhau profile screen" width="600"/>

### Note: you have two options when it comes to creating your whakapapa record, start with:

1. Another person OR
2. Import from a CSV file

### Import from a CSV file
If you have some whakapapa information or a tribal registrty already that you would like to put into Āhau it may be easier to do your data entry into a spreadsheet and upload the spreadsheet as a csv into Āhau.<br/>
Please [watch this video](https://drive.google.com/file/d/1rNH0QpOBsfYfHmMExjISlhzLqdfHqPLq/view?usp=sharing) to learn how to prepare a csv to upload into Āhau.

#### For the rest of this example we will be starting with *'Another person'*

## Seting access and permissions 

Set who can view your whakapapa record and who can edit it.

- The access to a whakapapa record is set based in the Tribe that you are in. 
- Currently you can choose whether a whakapapa record can be viewed by *'all members'* or by *'kaitkia only'*. 
- Whakapapa records by default are set to be **editable** by all members of the community. This is to make it possible for a community to contribute to the whakapapa record. If you do not want this you can set the record to *'view'* only.

<img src="_media/whakapapa_16.png" alt="Āhau profile screen" width="600"/>

## Add a person

Start with a parent or grandparent. Fill in as much or as little information as you like, you can always edit this later if you're unable to enter all the information straight away. 

<img src="_media/whakapapa_7.png" alt="Āhau profile screen" width="600"/>

Upload a photo of the person if you have one on file. 

*(NB: you can fill in as little or as detailed information as you like. You are also able to assign *'other'* to Takatāpui whānau or non-binary folks)*

<img src="_media/whakapapa_8.png" alt="Āhau profile screen" width="600"/>

Fill in any contact details you have available for this person. 

## Add more whānau members

### Add child

Once you have added your first whānau member, you will be able to see a screen similar to the following:

<img src="_media/whakapapa_9.png" alt="Āhau profile screen" width="600"/>

From here, you can add parents or children of that whānau member. Click on the arrow icon above a person and select *add child* to enter a child's information, and update the record. 

If you are the child of the person you created, you will be able to start typing your name and your Āhau profile will auto-populate and you will be able to select it.

<img src="_media/whakapapa_10.png" alt="Āhau profile screen" width="600"/>

Continue the process for adding children until all children are added.

<img src="_media/whakapapa_12.png" alt="Āhau profile screen" width="600"/>

### Add parent

<img src="_media/whakapapa_14.png" alt="Āhau profile screen" width="600"/>

Follow similar process to above to add parents.

### Add partner

Currently the only way to add a partner is to add a second parent to a child. 

### Deleting and sorting

Delete: If someone has been added to the record incorrectly they can be either hidden from this record or deleted if the have been created in error. Click on the arrow icon above a person and select Delete to remove a person and their children from the tree. Please be careful when deleting someone as that information cannot be brought back and will have to be added again. 

Sort: When adding a person, use order of birth to sort the name in order from 1st born - last born. 

## View your whakapapa record

Once you have filled in your whānau members to the whakapapa record.

<img src="_media/whakapapa_15.png" alt="Āhau profile screen" width="600"/>
## View your tribal registry

You can toggle your view to view the full list of people in your whakapapa tribal registry.

<img src="_media/whakapap-register-1.png" alt="Āhau profile screen" width="600"/>

Click on the *Registry* button in the top right-hand corner

<img src="_media/whakapap_register_buttons.png" alt="Āhau profile screen" width="600"/>

View the list of people in your tribal register.

<img src="_media/whakapapa-register-2.png" alt="Āhau profile screen" width="600"/>

You can then use the same process as above to edit profiles, and add more people to your tribal registry.

<img src="_media/whakapapa_registry_add_details.png" alt="Āhau profile screen" width="600"/>

You have now successfully created a whakapapa record. Why dont you try adding some stories about someone by either 
1. hovering over a person, clicking on 'view person' and then clicking on stories and adding a story, or 
2. going to your personal or tribal archive (depending on what access you set to for the whakapapa record), and then creating a story and mentioning this person. 

